#!/bin/bash

# https://rdf4j.org/documentation/reference/rest-api/#repository-queries
# https://zazuko.com/get-started/sparql-query/

# The -H "Accept: text/csv" sets the requested content-type to CSV, --data-urlencode query@sparql-query.rq uri-encodes the query and sends it to query on the endpoint, according to the SPARQL 1.1 Protocol. If you don't want to save it to a file, simply omit the -o swiss-offices.csv option and curl will echo the result on stdout.


curl -H "Accept: application/sparql-results+xml" --data-urlencode query@listTopLevel_public.rq http://localhost:9081/openrdf-sesame/repositories/textgrid-public -o toplevel-results.xml
