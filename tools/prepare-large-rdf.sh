#!/bin/bash

# exit and show help if no uri given
if [ "$#" == "0" ]; then
    echo -e "\e[31mno filename given\e[0m"
    #show_help    # Display a usage synopsis.
    exit
fi


WORKDIR=import_`basename $1`
#N3FILE=$WORKDIR/all.n3

mkdir $WORKDIR

#echo "rapper -i turtle -o ntriples $1 | split --line-bytes 20MB --additional-suffix .n3 - $WORKDIR/part_"
rapper -i turtle -o ntriples $1 | split --line-bytes 10MB --additional-suffix .n3 - $WORKDIR/part_


echo "done, you may want to do next: "
echo "for i in ${WORKDIR}/*.n3; do echo \$i; curl http://localhost:9081/openrdf-sesame/repositories/textgrid-[INSERT-HERE]/statements -X POST --header 'Content-Type: text/plain;charset=UTF-8' -T \$i; done"
