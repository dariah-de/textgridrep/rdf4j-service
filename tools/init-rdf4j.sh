#!/bin/bash

# https://rdf4j.org/documentation/reference/rest-api/#repository-creation

RDF_DB_PORT=${RDF_DB_PORT:-9081}
RDF_DB_PORT_INTERNAL=${RDF_DB_PORT_INTERNAL:-8080}

WORKBENCH_PATH="openrdf-workbench"
SERVER_PATH="openrdf-sesame"
#WORKBENCH_PATH="rdf4j-workbench"
#SERVER_PATH="rdf4j-server"

echo "[info] init public repo"
echo "[exec] curl -XPUT http://localhost:${RDF_DB_PORT}/${SERVER_PATH}/repositories/textgrid-public --header 'Content-Type: text/turtle;charset=UTF-8' --data @textgrid-public.config.ttl"
curl -XPUT http://localhost:${RDF_DB_PORT}/${SERVER_PATH}/repositories/textgrid-public --header 'Content-Type: text/turtle;charset=UTF-8' --data @textgrid-public.config.ttl
TTL_TARGET_PUBLIC="http://localhost:$RDF_DB_PORT/${SERVER_PATH}/repositories/textgrid-public/statements"
curl $TTL_TARGET_PUBLIC -X POST -d @mime.ttl --header 'Content-Type: text/turtle;charset=UTF-8'
echo "[exec] curl $TTL_TARGET_PUBLIC -X POST -d @mime.ttl --header 'Content-Type: text/turtle;charset=UTF-8'"

echo "[info] init nonpublic repo"
echo "[exec] curl -XPUT http://localhost:${RDF_DB_PORT}/${SERVER_PATH}/repositories/textgrid-nonpublic --header 'Content-Type: text/turtle;charset=UTF-8' --data @textgrid-nonpublic.config.ttl"
curl -XPUT http://localhost:${RDF_DB_PORT}/${SERVER_PATH}/repositories/textgrid-nonpublic --header 'Content-Type: text/turtle;charset=UTF-8' --data @textgrid-nonpublic.config.ttl
echo "[exec] curl $CREATE_TARGET --data ${CREATE_DATA_NONPUBLIC}"
TTL_TARGET_NONPUBLIC="http://localhost:$RDF_DB_PORT/${SERVER_PATH}/repositories/textgrid-nonpublic/statements"
curl $TTL_TARGET_NONPUBLIC -X POST -d @mime.ttl --header 'Content-Type: text/turtle;charset=UTF-8'
echo "[exec] curl $TTL_TARGET_NONPUBLIC -X POST -d @mime.ttl --header 'Content-Type: text/turtle;charset=UTF-8'"

echo "[info] init federated repo"
echo "[exec] curl -XPUT http://localhost:${RDF_DB_PORT}/${SERVER_PATH}/repositories/textgrid-federated --header 'Content-Type: text/turtle;charset=UTF-8' --data @textgrid-federated.config.ttl"
curl -XPUT http://localhost:${RDF_DB_PORT}/${SERVER_PATH}/repositories/textgrid-federated --header 'Content-Type: text/turtle;charset=UTF-8' --data @textgrid-federated.config.ttl

echo -e "\n\nDONE \nto restore a backup make sure to have enough RAM and enter something like:"
echo "    curl http://localhost:${RDF_DB_PORT}/${SERVER_PATH}/repositories/textgrid-nonpublic/statements -X POST --header 'Content-Type: text/turtle;charset=UTF-8' -T openrdf_textgrid-nonpublic_[...]"
echo "and"
echo "    curl http://localhost:${RDF_DB_PORT}/${SERVER_PATH}/repositories/textgrid-public/statements -X POST --header 'Content-Type: text/turtle;charset=UTF-8' -T openrdf_textgrid-public_[...]"
