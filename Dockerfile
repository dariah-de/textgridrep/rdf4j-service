FROM debian:bullseye as builder

ARG RDF4J_VERSION="4.1.3"
ARG JOLOKIA_VERSION="1.3.7" # jolokia as of 1.5.0 requires auth for proxy operation

RUN apt-get update && apt-get install -y wget
# rename rdf4j files to openrdf-sesame for backwards-compatibility on tg servers
RUN wget -q "https://repo1.maven.org/maven2/org/eclipse/rdf4j/rdf4j-http-workbench/${RDF4J_VERSION}/rdf4j-http-workbench-${RDF4J_VERSION}.war" -O /tmp/openrdf-workbench.war
RUN wget -q "https://repo1.maven.org/maven2/org/eclipse/rdf4j/rdf4j-http-server/${RDF4J_VERSION}/rdf4j-http-server-${RDF4J_VERSION}.war" -O /tmp/openrdf-sesame.war
RUN wget -q "https://repo1.maven.org/maven2/org/jolokia/jolokia-war/${JOLOKIA_VERSION}/jolokia-war-${JOLOKIA_VERSION}.war" -O /tmp/jolokia.war


#FROM tomcat:9-jdk17-openjdk-slim-bullseye
# as in https://github.com/eclipse/rdf4j/blob/main/docker/Dockerfile
FROM tomcat:8.5-jre11-temurin

ENV JAVA_OPTS="-Xmx768m"
ENV CATALINA_OPTS="-Dorg.eclipse.rdf4j.appdata.basedir=/var/rdf4j -Dcom.sun.management.jmxremote.port=9999 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"

RUN addgroup --system --gid 1020 tomcat
RUN adduser --system --uid 1020 --gid 1020 tomcat

WORKDIR /tmp

RUN rm -rf /usr/local/tomcat/webapps/* &&\
	  mkdir -p /var/rdf4j && \
    chown -R tomcat /var/rdf4j /usr/local/tomcat && \
    chmod a+x /usr/local/tomcat /usr/local/tomcat/bin /usr/local/tomcat/bin/catalina.sh

COPY --from=builder /tmp/*.war /usr/local/tomcat/webapps/

COPY web.xml /usr/local/tomcat/conf/web.xml
COPY server.xml /usr/local/tomcat/conf/server.xml

USER tomcat

WORKDIR /usr/local/tomcat/

EXPOSE 8080

