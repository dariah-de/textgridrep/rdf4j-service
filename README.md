# RDF4J dockerized for textgrid servers

The docker files in this repo are based on https://github.com/eclipse/rdf4j/tree/main/assembly/src/main/dist/docker.

The service paths are renamed to /openrdf-service/ and /openrdf-workbench/ for backwards compatibility. This may change.

# Start locally

Run

        docker-compose build
        docker-compose up


and access the workbench at http://localhost:9081/openrdf-workbench/ . 
The RDF4J server in the workbench is http://localhost:8080/openrdf-sesame and **NOT**
http://localhost:9081/openrdf-sesame as this is adressing the docker localhost 
port and not the hosts port.

# Tools

* The script [tools/init-rdf4j.sh](./tools/init-rdf4j.sh) initializes the default
textgrid RDF repositories.
* The script [tools/prepare-large-rdf.sh](./tools/prepare-large-rdf.sh)
is meant to split large backups in smaller chunks to ingest RAM friendly into the triplestore.
* The script [tools/querytest/toplevel.sh](./tools/querytest/toplevel.sh) is intended to send a rather long runnnig SPARQL request to a local store, containing a copy of public textgrid RDF. This is useful for performance comparision.


# Release process

We use git-flow.

        git checkout develop
        git flow release start 0.0.1
        git flow release finish 0.0.1
        git push origin develop main --tag

